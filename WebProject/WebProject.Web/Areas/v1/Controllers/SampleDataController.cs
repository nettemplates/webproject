﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebProject.Web.Areas.v1.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class SampleDataController : ControllerBase
	{
		/// <summary>
		/// Get Sample Data
		/// </summary>
		/// <param name="id">"1" or "2" or "3" or "4"</param>
		/// <returns>Array of ids</returns>
		[HttpGet]
		[ProducesResponseType(200, Type = typeof(ICollection<string>))]
		[ProducesResponseType(400, Type = typeof(string))]
		public async Task<IActionResult> GetData([FromQuery]string id = null)
		{
			var list = new List<string>() { "1","2","3","4"};
			var first = list.SingleOrDefault(x=>x==id);
			if (first == null)
			{
				return BadRequest("No data!");
			}
			return Ok(list);
		}

	}
}