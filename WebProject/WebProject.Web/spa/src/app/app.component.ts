import { Component } from '@angular/core';
import { SampleDataService } from './api/services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'spa';
  constructor(private _sampleDataService: SampleDataService) {
    _sampleDataService.ApiSampleDataGet("0").subscribe(data => {
      console.log(data);
    });
  }
}
