import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';

import { AppComponent } from './app.component';
import { ApiModule } from './api/api.module';
import { ApiConfiguration } from './api/api-configuration';
import { window } from 'rxjs/operators';
/**
 * Contains global configuration for API services
 */
@Injectable({
  providedIn: 'root',
})
export class ApiMockConfiguration {
  rootUrl: string = (<any>window).apiUrl;
}



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ApiModule
  ],
  providers: [{ provide: ApiConfiguration, useClass: ApiMockConfiguration }],
  bootstrap: [AppComponent]
})
export class AppModule { }

