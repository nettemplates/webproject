/* tslint:disable */
import { Injectable } from '@angular/core';
import {
  HttpClient, HttpRequest, HttpResponse,
  HttpHeaders, HttpParams } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
class SampleDataService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param id "1" or "2" or "3" or "4"
   * @return Success
   */
  ApiSampleDataGetResponse(id?: string): Observable<HttpResponse<Array<string>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (id != null) __params = __params.set('id', id.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/SampleData`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: Array<string> = null;
        _body = _resp.body as Array<string>;
        return _resp.clone({body: _body}) as HttpResponse<Array<string>>;
      })
    );
  }

  /**
   * @param id "1" or "2" or "3" or "4"
   * @return Success
   */
  ApiSampleDataGet(id?: string): Observable<Array<string>> {
    return this.ApiSampleDataGetResponse(id).pipe(
      map(_r => _r.body)
    );
  }
}

module SampleDataService {
}

export { SampleDataService }
