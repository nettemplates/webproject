﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace WebProject.Web.Controllers
{
	public class HomeController : Controller
	{
		private IHostingEnvironment _hostingEnvironment;

		public HomeController(IHostingEnvironment hostingEnvironment)
		{
			_hostingEnvironment = hostingEnvironment;
		}

		public IActionResult Index()
		{
			var apiUrl = string.Empty;
			if (_hostingEnvironment.IsDevelopment())
			{
				apiUrl = string.Format("{0}://{1}{2}{3}", Request.Scheme, Request.Host, Request.Path, Request.QueryString);
			}
			if (_hostingEnvironment.IsEnvironment("Production"))
			{
				apiUrl = "ProductionURL";
			}
			return View(apiUrl as object);
		}
	}
}
